<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = DB::table('acl_roles')->where('name', 'admin')->first();

        DB::table('users')->insert([
            'acl_role_id' => $role->id,
            'name' => 'demo',
            'email' => 'demo@altsolution.net',
            'active' => true,
            'password' => bcrypt('demo'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
