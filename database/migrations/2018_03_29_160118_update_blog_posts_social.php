<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBlogPostsSocial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_posts', function($table)
        {
            $table->integer('share_twitter_count')->default(0);
            $table->integer('share_facebook_count')->default(0);
            $table->integer('share_vkontakte_count')->default(0);
            $table->integer('share_telegram_count')->default(0);
            $table->integer('share_pinterest_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_posts', function (Blueprint $table) {
            $table->dropColumn(['share_twitter_count', 'share_facebook_count', 'share_vkontakte_count', 'share_telegram_count', 'share_pinterest_count']);
        });
    }
}
