<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostBlogSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post_blog_section', function (Blueprint $table) {
            $table->integer('blog_post_id')->unsigned();
            $table->foreign('blog_post_id')->references('id')
                ->on('blog_posts');

            $table->integer('blog_section_id')->unsigned();
            $table->foreign('blog_section_id')->references('id')
                ->on('blog_sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_post_blog_section');
    }
}
