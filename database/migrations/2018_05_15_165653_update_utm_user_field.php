<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUtmUserField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('utm_users', function (Blueprint $table) {
            $table->text('utm_arr')->nullable()->change();
            $table->integer('roistat_visit')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('utm_users', function (Blueprint $table) {
            $table->text('utm_arr')->change();
            $table->integer('roistat_visit')->change();
        });
    }
}
