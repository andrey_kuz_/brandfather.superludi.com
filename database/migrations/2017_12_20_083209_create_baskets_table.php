<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baskets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable()->default(null);
			$table->integer('video_id')->unsigned()->nullable()->default(null);
			
			$table->unique(['order_id', 'video_id']);
			$table->foreign('order_id', 'fk_baskets_orders')
                ->references('id')->on('orders')
                ->onUpdate('cascade')
                ->onDelete('cascade');
			$table->foreign('video_id', 'fk_baskets_videos')
                ->references('id')->on('videos')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('baskets');
    }
}
