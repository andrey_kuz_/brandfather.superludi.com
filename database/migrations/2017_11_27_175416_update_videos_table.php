<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
			$locale = config('app.locale');
			if(!empty($locale))
			{
				$table->string('text_short_' . $locale)->after('title_'.$locale); 
			}	
	   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
			$locale = config('app.locale');
			if(!empty($locale))
			{
				$table->dropColumn('text_short_' . $locale);
			}	
		});
    }
}
