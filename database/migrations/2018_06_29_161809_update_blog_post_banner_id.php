<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBlogPostBannerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_posts', function($table)
        {
            $table->unsignedInteger('banner_id')->nullable();
            $table->index('banner_id');
            $table->foreign('banner_id')->references('id')->on('blog_banners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_posts', function($table)
        {
            $table->dropIndex('banner_id');
            $table->dropForeign('blog_posts_banner_id_foreign');
            $table->dropColumn('banner_id');

        });
    }
}
