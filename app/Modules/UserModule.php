<?php

namespace App\Modules;

use \AltSolution\Admin\Modules\UserModule as BaseModule;
use Illuminate\Routing\Router;

class UserModule extends BaseModule
{
    public function registerRoutes(Router $router)
    {
        parent::registerRoutes($router);

        $namespace = 'App\Http\Controllers\Admin';
        $router->group(['namespace' => $namespace], function(Router $router)
        {
            $router->get('users/edit/{id?}', 'UsersController@getEdit')->name('user_edit');
            $router->post('users/save', 'UsersController@postSave')->name('user_save');
			$router->get('users/delete/{id?}', 'UsersController@getDelete')->name('user_delete');
			$router->post('users/massdelete', 'UsersController@postMassdelete')->name('user_massdelete');
            $router->post('users/reset', 'UsersController@postReset')->name('user_reset');
        });
    }
}