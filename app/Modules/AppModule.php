<?php namespace App\Modules;

use AltSolution\Admin\Models\Content;
use AltSolution\Admin\System\AbstractModule;
use AltSolution\Admin\System\EntityBuilder;
use AltSolution\Admin\System\MenuBuilder;
use AltSolution\Admin\System\PermissionBuilder;
use AltSolution\Admin\System\SeoBuilder;

class AppModule extends AbstractModule
{
    public function buildMenu(MenuBuilder $menu)
    {
        $w = 1000;

        $menu->addSection(trans('admin.m_video'), 'fa-video-camera')
            ->setAlias('video')
            ->setWeight(--$w);
        $menu->addItem(trans('admin.mi_video_list'), route('admin/video_list'))
            ->setPermission('video.list');

        $menu->addSection(trans('admin.m_order'), 'fa-newspaper-o')
            ->setAlias('order')
            ->setWeight(--$w);
        $menu->addItem(trans('admin.mi_order_list'), route('admin/order_list'))
            ->setPermission('order.list');

        $menu->addSection(trans('admin.m_blog'), 'fa-book')
            ->setAlias('blog')
            ->setWeight(--$w);
        $menu->addItem(trans('admin.mi_blog_section_list'), route('admin/section_list'))
            ->setPermission('blog.control');
        $menu->addItem(trans('admin.mi_blog_post_list'), route('admin/post_list'))
            ->setPermission('blog.control');
        $menu->addItem(trans('admin.mi_blog_banner_list'), route('admin/banner_list'))
            ->setPermission('blog.control');
    }

    public function buildPermissions(PermissionBuilder $builder)
    {
        $builder->addSection('admin.ps_video');
        $builder->addPermission('video.list', 'admin.p_video_list');
        $builder->addPermission('video.edit', 'admin.p_video_edit');
        $builder->addPermission('video.delete', 'admin.p_video_delete');
        $builder->addPermission('blog.control', 'blog.control');
        $builder->addSection('admin.ps_order');
        $builder->addPermission('order.list', 'admin.p_order_list');
    }

    public function buildEntity(EntityBuilder $builder)
    {
        $builder->addRoute('admin.me_video_list', 'video_list')
            ->setWeight(10);

        $builder->addRoute('admin.me_order_list', 'order_list')
            ->setWeight(11);
        $builder->addRoute('admin.me_section_list', 'section_list')
            ->setWeight(12);

        $builder->addModel('admin.me_content_view', Content::class, 'content')
            ->setModelKey('url')
            ->setWeight(-1);
    }

    public function buildSeo(SeoBuilder $builder)
    {
        $builder->addSection('video', 'admin.ss_video')
            ->setWeight(9);
        $builder->addDefaultFields(':year - current year');
        $builder->addField('text', 'admin.s_video_text')->setType('wysiwyg');
    }
}