<?php
/**
 * Created by PhpStorm.
 * User: aku
 * Date: 23.03.18
 * Time: 14:02
 */

namespace App\Forms;
use AltSolution\Admin\Form;
use AltSolution\Admin\Form\Field;
use AltSolution\Admin\Form\Component;
use App\Models\BlogBanner;
use App\Models\BlogSection;
use Carbon\Carbon;

class PostSaveForm extends Form\AbstractFactory
{
    public function buildForm(Form\BuilderInterface $builder)
    {
        $builder->add('form_open', Component\FormOpen::class, [
            'method' => 'post',
            'action' => route('admin/post_save'),
            'enctype' => 'multipart/form-data',
        ]);
        $builder->add('form_submit', Component\FormSubmit::class);
        $builder->add('form_close', Component\FormClose::class);

        $builder->add('title', Field\Text::class, [
            'label' => trans('admin/post.title'),
            'required' => true,
        ]);

        $builder->add('author', Field\Text::class, [
            'label' => trans('admin/post.author'),
            'required' => true,
        ]);

        $builder->add('image', Field\Image::class, [
            'label' => trans('admin/post.image_title'),
            'required' => true,
        ]);

        $builder->add('slug', Field\Text::class, [
            'label' => trans('admin/post.slug'),
            'class' =>'tinymce-elem',
           /* 'required' => true,*/
        ]);
        $builder->add('content', Field\Textarea::class, [
            'label' => trans('admin/post.content'),
        ]);
        $builder->add('date', Field\Date::class, [
            'label' => trans('admin/post.content'),
            'format' => 'm/d/Y',
        ]);
        $builder->add('section', Field\SelectModel::class, [
            'label' => trans('admin/post.section'),
            'model' => BlogSection::class,
            'required' => true,
        ]);
        $builder->add('banner_id', Field\SelectModel::class, [
            'label' => trans('admin/post.banner'),
            'model' => BlogBanner::class,
            'required' => true,
        ]);
        $builder->add('draft', Field\Checkbox::class, [
            'label' => trans('admin/post.draft'),
        ]);


    }

}