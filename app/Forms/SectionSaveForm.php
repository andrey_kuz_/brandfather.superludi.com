<?php
/**
 * Created by PhpStorm.
 * User: aku
 * Date: 23.03.18
 * Time: 14:02
 */

namespace App\Forms;
use AltSolution\Admin\Form;
use AltSolution\Admin\Form\Field;
use AltSolution\Admin\Form\Component;

class SectionSaveForm extends Form\AbstractFactory
{
    public function buildForm(Form\BuilderInterface $builder)
    {
        $builder->add('form_open', Component\FormOpen::class, [
            'method' => 'post',
            'action' => route('admin/section_save'),
        ]);
        $builder->add('form_submit', Component\FormSubmit::class);
        $builder->add('form_close', Component\FormClose::class);

        $builder->add('title', Field\Text::class, [
            'label' => trans('admin/section.title'),
            'required' => true,
        ]);

        $builder->add('slug', Field\Text::class, [
            'label' => trans('admin/section.slug'),
            /*'required' => true,*/
        ]);
    }

}