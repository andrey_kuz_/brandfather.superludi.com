<?php
/**
 * Created by PhpStorm.
 * User: aku
 * Date: 23.03.18
 * Time: 14:02
 */

namespace App\Forms;
use AltSolution\Admin\Form;
use AltSolution\Admin\Form\Field;
use AltSolution\Admin\Form\Component;
use App\Models\BlogSection;
use Carbon\Carbon;

class BannerSaveForm extends Form\AbstractFactory
{
    public function buildForm(Form\BuilderInterface $builder)
    {
        $builder->add('form_open', Component\FormOpen::class, [
            'method' => 'post',
            'action' => route('admin/banner_save'),
            'enctype' => 'multipart/form-data',
        ]);
        $builder->add('form_submit', Component\FormSubmit::class);
        $builder->add('form_close', Component\FormClose::class);

        $builder->add('title', Field\Text::class, [
            'label' => trans('admin/banner.title'),

        ]);
        $builder->add('image', Field\Image::class, [
            'label' => trans('admin/banner.image'),
            'required' => true,
        ]);
        $builder->add('mobile_image', Field\Image::class, [
            'label' => trans('admin/banner.mobile_image'),
            'required' => true,
        ]);
        $builder->add('link', Field\Text::class, [
            'label' => trans('admin/banner.link'),
        ]);
        $builder->add('main_page', Field\Checkbox::class, [
            'label' => trans('admin/banner.main_page'),
        ]);
    }

}