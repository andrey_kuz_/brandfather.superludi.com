<?php

namespace App\Forms;

use AltSolution\Admin\Form;
use AltSolution\Admin\Form\Field;
use AltSolution\Admin\Form\Component;
use AltSolution\Admin\Models\AclRole;

class UserForm extends Form\AbstractFactory
{
    public function buildForm(Form\BuilderInterface $builder)
    {
        $builder->add('form_open', Component\FormOpen::class, [
            'method' => 'post',
            'action' => route('admin::user_save'),
            'enctype' => 'multipart/form-data',
        ]);
        $builder->add('form_submit', Component\FormSubmit::class);
        $builder->add('form_close', Component\FormClose::class);

        $builder->add('id', Field\Hidden::class);
        $builder->add('avatar_file', Field\Image::class, [
            'label' => trans('admin::user.avatar'),
            'help' => trans('admin::user.avatar_description'),
        ]);
        $builder->add('name', Field\Text::class, [
            'label' => trans('admin::user.username'),
            'required' => true,
        ]);
        $builder->add('email', Field\Email::class, [
            'label' => trans('admin::user.email'),
            'required' => true,
        ]);
		$builder->add('phone', Field\Text::class, [
            'label' => trans('admin/user.phone'),
            'required' => true,
        ]);
        $builder->add('password', Field\Password::class, [
            'label' => trans('admin::user.password'),
            'different' => 'name',
        ]);
        $builder->add('password_confirmation', Field\Password::class, [
            'label' => trans('admin::user.confirm_password'),
            'identical' => 'password',
        ]);
        $builder->add('active', Field\Checkbox::class, [
            'label' => trans('admin::user.activate_question'),
            'placeholder' => trans('admin::common.activate'),
            'help' => trans('admin::user.activate_description'),
        ]);
        $builder->add('paid', Field\Checkbox::class, [
            'label' => trans('admin/user.paid_question'),
            'placeholder' => trans('admin/user.paid'),
            'help' => trans('admin/user.paid_description'),
        ]);

        $builder->add('package_name', Field\Radio::class, [
            'label' => trans('admin/user.package_name'),
            'choices' =>  ['GOOD' => 'GOOD', 'BEST' => 'BEST', 'PREMIUM' => 'PREMIUM'],
            'title_key' => 'description',
        ]);

        $builder->add('acl_role_id', Field\SelectModel::class, [
            'label' => trans('admin::user.acl_role'),
            'model' => AclRole::class,
            'title_key' => 'description',
        ]);

        /*$builder->add('promocode', Field\Text::class, [
            'label' => trans('admin/user.promocode'),
            'placeholder' => trans('admin/user.promocode'),
        ]);*/

    }
}