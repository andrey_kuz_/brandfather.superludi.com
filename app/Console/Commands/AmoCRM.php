<?php

namespace App\Console\Commands;

use App\Services\AmoCrmService;
use Illuminate\Console\Command;

class AmoCRM extends Command
{
    protected $signature = 'crm:update';
    protected $description = 'Amo CRM updater';
    private $crmService;

    public function __construct(AmoCrmService $amoCrmService)
    {
        parent::__construct();
        $this->crmService = $amoCrmService;
    }

    public function handle()
    {
        $this->crmService->syncUsers();
        $this->crmService->saveLeadActions();
        $this->crmService->saveContactsActions();
        $this->crmService->updateLeadsActions();
        $this->info('Crm update');
    }
}
