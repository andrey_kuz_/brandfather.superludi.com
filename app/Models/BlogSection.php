<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BlogPost;

class BlogSection extends Model
{
    protected $fillable = ['title'];

    public function posts()
    {
        return $this->belongsToMany(BlogPost::class);
    }
}