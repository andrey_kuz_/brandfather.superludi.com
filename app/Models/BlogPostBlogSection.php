<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPostBlogSection extends Model
{
    protected $table = 'blog_post_blog_section';
    public $timestamps = false;
}
