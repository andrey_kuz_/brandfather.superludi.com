<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use AltSolution\Admin\Helpers\ImagesInterface;
use AltSolution\Admin\Helpers\ImagesTrait;
use AltSolution\Admin\Helpers\UploadsInterface;
use AltSolution\Admin\Helpers\UploadsTrait;
use AltSolution\Admin\Helpers\SeoInterface;
use AltSolution\Admin\Helpers\SeoTrait;
use AltSolution\Admin\Helpers\TranslateTrait;


class BlogBanner extends Model implements SeoInterface, ImagesInterface
{
    use ImagesTrait;
    use UploadsTrait;
    use TranslateTrait;
    use SeoTrait;

    protected $fillable = ['id','title', 'image', 'link', 'main_page'];

    public function getImagesFields()
    {
        return [
            'image' => [
                'list' => ['resize', 1920, 1920],
                'list_public' => ['crop', 1920, 1920],
            ],
            'mobile_image' => [
                'list' => ['resize', 1024, 1024],
                'list_public' => ['crop', 1024, 1024],
            ],
        ];
    }
    public function posts()
    {
        return $this->hasMany(BlogPost::class);
    }

}