<?php

namespace App\Models;

use AltSolution\Admin\Helpers\ImagesInterface;
use AltSolution\Admin\Helpers\ImagesTrait;
use AltSolution\Admin\Helpers\UploadsInterface;
use AltSolution\Admin\Helpers\UploadsTrait;
use AltSolution\Admin\Helpers\SeoInterface;
use AltSolution\Admin\Helpers\SeoTrait;
use AltSolution\Admin\Helpers\TranslateTrait;
use Illuminate\Database\Eloquent\Model;

class Video extends Model implements SeoInterface, ImagesInterface, UploadsInterface
{
    use ImagesTrait;
	use UploadsTrait;
    use TranslateTrait;
    use SeoTrait;

	static public $sortBy = 'sort';
	static public $sortDir = 'asc';

	protected $fillable = [
        'title_*',
        'text_short_*',
		'content_*',
        'permalink',
		'image',
		'file',
		'code',
		'is_free',
        'is_published',
    ];

    public function getImagesFields()
    {
        return [
            'image' => [
                'list' => ['resize', 213, 124],
                'list_public' => ['crop', 640, 372],
            ],
        ];
    }
	
	public function getUploadsFields()
    {
        return ['file'];
    }
	
	public function scopeAvailable($query) {
        return $query->where('is_published', 1);
    }
	
	public function scopeFree($query) {
        return $query->where('is_free', 1);
    }
	
	public function scopeSale($query) {
        return $query->where('is_free', 0);
    }
	
	public function scopeSorting($query) {
        return $query->orderBy(self::$sortBy, self::$sortDir);
    }
	
    public function scopeSearch($query, $text)
    {
        return $query->where(function ($query) use ($text) {
            $locale = config('app.locale');
            $query->orWhere('title_' . $locale, 'LIKE', '%' . $text . '%');
			$query->orWhere('text_short_' . $locale, 'LIKE', '%' . $text . '%');
			$query->orWhere('content_' . $locale, 'LIKE', '%' . $text . '%');
        });
    }
	
	public function daysLeft()
	{
		return cms_option('course_expires');
	}
	
	public function index($number)
	{
		return str_pad($number, 2, '0', STR_PAD_LEFT);
	}
	
	static public function videosAvailable()
	{
		$videos = Video::available()->sale()
				->sorting()
				->get()
				;
		
		return $videos;
	}
}
