<?php

namespace App\Models;
use Carbon\Carbon;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\BlogSection;
use AltSolution\Admin\Helpers\ImagesInterface;
use AltSolution\Admin\Helpers\ImagesTrait;
use AltSolution\Admin\Helpers\UploadsInterface;
use AltSolution\Admin\Helpers\UploadsTrait;
use AltSolution\Admin\Helpers\SeoInterface;
use AltSolution\Admin\Helpers\SeoTrait;
use AltSolution\Admin\Helpers\TranslateTrait;
use Sunra\PhpSimple\HtmlDomParser;

class BlogPost extends Model implements SeoInterface, ImagesInterface, UploadsInterface
{
    use ImagesTrait;
    use UploadsTrait;
    use TranslateTrait;
    use SeoTrait;

    protected $fillable = ['id','title','author','image','slug','content','view_count','follow_count','date','draft','banner_id'];

    public function setDateAttribute($value) {
//        print_r($value);
//        echo '</br>';
//        print_r(strtotime($value));
//        echo '</br> ';
//        print_r(date("Y-m-d H:i:s",strtotime($value)));
//        exit();
        $this->attributes['date'] = date("Y-m-d H:i:s",strtotime($value));
    }

    public function getImagesFields()
    {
        return [
            'image' => [
                'list' => ['resize', 1024, 600],
                'list_public' => ['crop', 1024, 600],
            ],
        ];
    }

    public function sections()
    {
        return $this->belongsToMany(BlogSection::class);
    }


    public function getSectionsIdsAttribute()
    {
        return $this->sections()->pluck('id');
    }

    public function banner()
    {
        return $this->belongsTo(BlogBanner::class);
    }

    /**
     * @param $request
     */
    public function add($request) {
        $request->view_count = 0;
        $this->fill($request->all());
        $this->imageAllSave($request);
        if (!$request->slug) {
            $this->slug = str_slug($request->title);
        }
        $this->save();
        $sections = $request->section;
        foreach ($sections as $section) {
            $post_section =  new BlogPostBlogSection;
            $post_section->blog_post_id = $this->id;
            $post_section->blog_section_id = $section;
            $post_section->save();
        }
    }

    /**
     * @param $request
     */
    public function update_post($request) {

        $this->fill($request->all());
        $this->imageAllSave($request);
        if (!$request->slug) {
            $this->slug = str_slug($request->title);
        }
        $this->save();
        if($request->sections_ids) {
            BlogPostBlogSection::where('blog_post_id',$this->id)->delete();
            $sections = $request->sections_ids;
            foreach ($sections as $section) {
                $post_section = new BlogPostBlogSection;
                $post_section->blog_post_id = $this->id;
                $post_section->blog_section_id = $section;
                $post_section->save();
            }
        }
    }

    /**
     * @param $query
     * @param $keyword
     * @return mixed
     */
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("title", "LIKE","%$keyword%")
                    ->orWhere("slug", "LIKE", "%$keyword%")
                    ->orWhere("content", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

}
