<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempUserPack extends Model
{
    protected $table = 'temp_user_pack';
    protected $fillable = [
        'id_user',
        'name_pack'
    ];
}
