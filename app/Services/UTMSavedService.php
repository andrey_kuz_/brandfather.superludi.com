<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\UTMUsers;
use Illuminate\Support\Facades\Cookie;

class UTMSavedService
{
    public function __construct(Request $request)
    {
        if (\Auth::check()) {
            $this->saveUTM(\Auth::user()->id);
        }
    }

    private function saveUTM($user_id)
    {
        $utm_source = null;
        $utm_medium = null;
        $utm_campaign = null;
        $utm_term = null;
        $utm_content = null;
        $roistat_visit = null;

        if (isset($_COOKIE['utm_source'])) {
            $utm_source = $_COOKIE['utm_source'];
        }
        if (isset($_COOKIE['utm_medium'])) {
            $utm_medium = $_COOKIE['utm_medium'];
        }
        if (isset($_COOKIE['utm_campaign'])) {
            $utm_campaign = $_COOKIE['utm_campaign'];
        }
        if (isset($_COOKIE['utm_term'])) {
            $utm_term = $_COOKIE['utm_term'];
        }
        if (Cookie::has('utm_content')) {
            $utm_content = Cookie::get('utm_content');
        }
        if (Cookie::has('roistat_visit')) {
            $roistat_visit = Cookie::get('roistat_visit');
        }

        if ($utm_source == null && $utm_medium == null && $utm_campaign == null && $utm_term == null && $utm_content == null && $roistat_visit == null)
            return;

        $jsonUtm = null;

        if ($utm_source != null || $utm_medium != null || $utm_campaign != null || $utm_term != null || $utm_content != null) {
            $arrUtm = [
                'utm_source' => $utm_source,
                'utm_medium' => $utm_medium,
                'utm_campaign' => $utm_campaign,
                'utm_term' => $utm_term,
                'utm_content' => $utm_content
            ];
            $jsonUtm = json_encode($arrUtm);
        }

        $user_utm = UTMUsers::query()->firstOrNew(['user_id' => $user_id]);

        if ($user_utm->status == 1)
            return;

            $user_utm->user_id = $user_id;
        if ($user_utm->utm_arr == null) {
            $user_utm->utm_arr = $jsonUtm;
        }

        if ($user_utm->roistat_visit == null) {
            $user_utm->roistat_visit = $roistat_visit;
        }
        if (($jsonUtm != null || $user_utm->utm_arr != null) && ($roistat_visit != null || $user_utm->roistat_visit != null)) {
            $user_utm->status = 1;
        }
        $user_utm->save();
    }
}