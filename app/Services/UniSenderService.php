<?php
/**
 * Created by PhpStorm.
 * User: aku
 * Date: 14.07.18
 * Time: 11:35
 */

namespace App\Services;

use omgdef\unisender\UniSenderWrapper;


class UniSenderService
{
    public function send($email) {

        $obj = new UniSenderWrapper();
        $obj->apiKey = env('UNI_SENDER_API_KEY');

        $list_id = env('UNI_SENDER_LIST_ID');
        $fields['email'] = $email;
        $params['double_optin'] = 3;

        $obj->subscribe($list_id, $fields, $params);

    }
}