<?php

namespace App;

interface Currency
{
    const UKRAINE = 'UAH';
    const RUSSIAN = 'RUB';
    const EURO = 'EUR';
    const USA = 'USD';
    const BITCOIN = 'BTC';
}