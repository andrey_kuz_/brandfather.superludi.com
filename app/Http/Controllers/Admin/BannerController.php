<?php

namespace App\Http\Controllers\Admin;

use App\Forms\BannerSaveForm;
use App\Forms\BannerUpdateForm;
use App\Models\BlogBanner;
use Illuminate\Http\Request;

use App\Http\Requests;
use AltSolution\Admin\Http\Controllers\Controller;

class BannerController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(Request $request)
    {
        $this->authorize('permission', 'blog.control');

        $q = BlogBanner::query();

        $filter = [
            'title' => $request->input('title'),
            'sort' => $request->input('sort', 'id-desc'),
        ];

        if (!empty($filter['title'])) {
            $q->where('title', $filter['title']);
        }

        list($sortBy, $sortDir) = explode('-', $filter['sort']);
        $q->orderBy($sortBy, $sortDir);

        $items = $q->paginate(config('admin.item_per_page', 10));
        $items->appends($request->input());
        $this->layout
            ->setActiveSection('blog')
            ->setTitle(trans('admin/banner.list'));

        return view('admin/banner.list', compact('items', 'filter'));
    }

    public function add()
    {
        return $this->index();
    }
    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id = null)
    {
        $this->authorize('permission', 'blog.control');
        $section = null;

        if ($id) {
            $section = BlogBanner::query()->find($id);
            $form = app(BannerUpdateForm::class)->create($section);
        }
        else {
            $form = app(BannerSaveForm::class)->create($section);
        }
        $data = [
            'edit_section' => $section,
            'form' => $form,
        ];
        $this->layout
            ->setActiveSection('blog')
            ->setTitle(trans($section ? 'admin/banner.edit' : 'admin/banner.add'))
            ->addBreadcrumb(trans('admin/banner.list'), route('admin/banner_list'));
        if ($id) {
            return view('admin/banner.edit', $data);
        }
        return view('admin/banner.add', $data);
    }

    /**
     * @param Request $request
     */
    public function save (Request $request)
    {
        $this->validate($request,[
            'title' => 'string|max:255',
            'image' => 'required|image',
            'mobile_image' => 'required|image',
            'link' => 'string|max:255',
            'main_page' =>'boolean',
        ]);
        $banner = new BlogBanner;
        if ($request->main_page) {
            $ids_main = BlogBanner::where('main_page', true)->get();

        }
        $banner->fill($request->all());
        $banner->imageAllSave($request);
        $banner->save();

        return redirect()->route('admin/banner_list');
    }

    /**
     * @param Request $request
     * @param $id integer
     */
    public function update (Request $request)
    {
        $this->validate($request,[
            'id' => 'required|exists:blog_banners|integer',
            'title' => 'string|max:255',
            'image' => 'image',
            'mobile_image' => 'image',
            'link' => 'string|max:255',
            'main_page' =>'boolean',
        ]);
        $banner = BlogBanner::find($request->id);
        if ($request->main_page) {
            BlogBanner::where('id','>',0)
                ->update(['main_page' => 0]);
        }
        $banner->fill($request->all());
        $banner->imageAllSave($request);
        $banner->save();
        return redirect()->route('admin/banner_list');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        BlogBanner::where('id', $id)->delete();
        return redirect()->route('admin/section_list');
    }

    /**
     * @param Request $request
     */
    public function action(Request $request)
    {
        $this->authorize('permission', 'blog.control');
        $action = $request->input('action');
        $itemIds = $request->input('ids');
        if (!$itemIds) {
            return;
        }
        foreach ($itemIds as $itemId) {
            $item = BlogBanner::query()->findOrFail($itemId);
            switch ($action) {
                /*case 'publish':
                    $item['is_published'] = true;
                    $item->published_at = new Carbon;
                    $item->save();
                    break;
                case 'hide':
                    $item['is_published'] = false;
                    $item->save();
                    break;*/
                case 'delete':
                {
                    $item->imageAllDelete();
                    $item->delete();
                }
                    break;
            }
        }
    }
}
