<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use AltSolution\Admin\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('permission', 'order.list');
		
        $q = Order::query();

        $filter = [
			'order_number' => $request->input('order_number'),
			'payed' => $request->input('payed'),
			'sort' => $request->input('sort', 'id-desc'),
        ];

        if (!empty($filter['order_number'])) {
            $q->where('order_number', $filter['order_number']);
        }
		
		if ($filter['payed']) {
            $q->where('payed', intval($filter['payed'] == 1));
        }

        list($sortBy, $sortDir) = explode('-', $filter['sort']);
        $q->orderBy($sortBy, $sortDir);

        $items = $q->paginate(config('admin.item_per_page', 50));
		$items->appends($request->input());
		
        $this->layout
            ->setActiveSection('order')
            ->setTitle(trans('admin/order.list'));

        return view('admin/order.list', compact('items', 'filter'));
    }

    public function history($id = null)
    {
        $this->authorize('permission', 'order.list');
		
        $order = Order::query()->find($id);
        if(empty($order))
		{
			abort(404);
		}	
        
        $this->layout
            ->setActiveSection('order')
            ->setTitle(trans('admin/order.history'))
            ->addBreadcrumb(trans('admin/order.list'), route('admin/order_list'));
        return view('admin/order.history', compact('order'));

    }

}
