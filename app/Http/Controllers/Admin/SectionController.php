<?php

namespace App\Http\Controllers\Admin;

use App\Forms\SectionSaveForm;
use App\Models\BlogSection;
use Illuminate\Http\Request;
use AltSolution\Admin\Http\Controllers\Controller;
use App\Forms\SectionUpdateForm;
use App\Models\BlogPostBlogSection;

class SectionController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(Request $request)
    {
        $this->authorize('permission', 'blog.control');

        $q = BlogSection::query();

        $filter = [
            'title' => $request->input('title'),
            'sort' => $request->input('sort', 'id-desc'),
        ];

        if (!empty($filter['title'])) {
            $q->where('title', $filter['title']);
        }

        list($sortBy, $sortDir) = explode('-', $filter['sort']);
        $q->orderBy($sortBy, $sortDir);

        $items = $q->paginate(config('admin.item_per_page', 50));
        $items->appends($request->input());
        $this->layout
            ->setActiveSection('blog')
            ->setTitle(trans('admin/section.list'));

        return view('admin/section.list', compact('items', 'filter'));
    }

    public function add()
    {
        return $this->index();
    }
    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id = null)
    {
        $this->authorize('permission', 'blog.control');
        $section = null;

        if ($id) {
            $section = BlogSection::query()->find($id);
            $form = app(SectionUpdateForm::class)->create($section);
        }
        else {
            $form = app(SectionSaveForm::class)->create($section);
        }
        $data = [
            'edit_section' => $section,
            'form' => $form,
        ];
        $this->layout
            ->setActiveSection('blog')
            ->setTitle(trans($section ? 'admin/section.edit' : 'admin/section.add'))
            ->addBreadcrumb(trans('admin/section.list'), route('admin/section_list'));
        if ($id) {
            return view('admin/section.edit', $data);
        }
        return view('admin/section.add', $data);
    }

    /**
     * @param Request $request
     */
    public function save (Request $request)
    {
        $this->validate($request,[
            'title' => 'required|unique:blog_sections|max:255',
            'slug' => 'unique:blog_sections|max:255',
        ]);
        $section = new BlogSection ;
        $section->title = $request->title;
        if($request->slug) {
            $section->slug = $request->slug;
        } else {
            $section->slug = str_slug($request->title);
        }
        $section->save();
        return redirect()->route('admin/section_list');
    }

    /**
     * @param Request $request
     * @param $id integer
     */
    public function update (Request $request)
    {
        $this->validate($request,[
            'id' => 'required|exists:blog_sections|integer',
            'title' => 'required|unique:blog_sections,title,'.$request->id.'|max:255',
            'slug' => 'unique:blog_sections,slug,'.$request->id.'|max:255',
        ]);
        $section = BlogSection::find($request->id);
        $section->title = $request->title;
        if($request->slug) {
            $section->slug = $request->slug;
        } else {
            $section->slug = str_slug($request->title);
        }
        $section->save();
        return redirect()->route('admin/section_list');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        BlogPostBlogSection::where('blog_section_id', $id)->delete();
        BlogSection::where('id', $id)->delete();
        return redirect()->route('admin/section_list');
    }

    public function action(Request $request)
    {
        $this->authorize('permission', 'blog.control');

        $action = $request->input('action');
        $itemIds = $request->input('ids');
        if (!$itemIds) {
            return;
        }
        foreach ($itemIds as $itemId) {
            $item = BlogSection::query()->findOrFail($itemId);
            switch ($action) {
                case 'delete':
                {
                    BlogPostBlogSection::where('blog_section_id',$itemId)->delete();
                    $item->delete();
                }
                    break;
            }
        }
    }
}
