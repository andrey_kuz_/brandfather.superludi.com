<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use AltSolution\Admin\Http\Controllers\OptionsController as BaseOptionsController;
use AltSolution\Admin\Models\Option;
use Validator;
use App\Models\Package;

class OptionsController extends BaseOptionsController
{
	public function getIndex()
    {
        $this->authorize('permission', 'options');

        $options = cms_options();
		$packages = Package::all();
		
        $this->layout
            ->setActiveSection('options')
            ->setTitle(trans('admin::option.title'));
    	return view('admin/options.list', compact('options', 'packages'));
    }
	
	public function postIndex(Request $request)
    {
        $this->authorize('permission', 'options');
		
		foreach(Package::all() as $package)
		{
			$data['course_package_'.$package['code'].'_price'] = $request->input('course_package_'.$package['code'].'_price');
		}
		
        $data['course_expires'] = $request->input('course_expires');
		$data['course_price_rate'] = $request->input('course_price_rate');
		$data['admin_email'] = $request->admin_email;
        foreach (explode(',', $request->admin_email_d) as $extraEmail) {
            if (trim($extraEmail)) {
                $data['admin_email_d'][] = trim($extraEmail);
            }
        }
		
		$rules = [
			'admin_email' => 'required|email',
			'admin_email_d.*' => 'email',
			'course_expires' => 'digits_between:1,3650',
			'course_price_rate' => 'required|numeric',
		];
		foreach(Package::all() as $package)
		{
			$rules['course_package_'.$package['code'].'_price'] = 'numeric';
		}
        $validator = Validator::make(
			$data,
			$rules
		);
		
		$rules_name = [
			'course_expires' => trans('admin/option.course_expires'),
			'course_price_rate' => trans('admin/option.course_price_rate'),
		];
		foreach(Package::all() as $package)
		{
			$rules_name['course_package_'.$package['code'].'_price'] = trans('admin/option.course_package_price').' '.$package['name'];
		}
		$validator->setAttributeNames($rules_name);

        if ($validator->fails()) {
            return redirect('admin/options')->withErrors($validator)->withInput();
        }


        foreach ($request->input() as $key => $value) {
            if ($key != '_token') {
                $option = Option::firstOrNew(['name' => $key]);
                $option->value = $value;
                $option->save();
            }
        }

        $this->layout->addNotify('success', trans('admin::option.saved'));

        return redirect('admin/options');
    }
}
