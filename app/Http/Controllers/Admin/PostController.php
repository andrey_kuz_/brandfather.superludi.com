<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogPost;
use App\Models\BlogPostBlogSection;
use Illuminate\Http\Request;
use App\Http\Requests;
use AltSolution\Admin\Http\Controllers\Controller;
use App\Forms\PostSaveForm;
use App\Forms\PostUpdateForm;

class PostController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(Request $request) {

        $this->authorize('permission', 'blog.control');

        $q = BlogPost::query();

        $filter = [
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'date' => $request->input('date'),
            'sort' => $request->input('sort', 'id-desc'),
        ];

        if (!empty($filter['title'])) {
            $q->where('title', $filter['title']);
        }

        if ($filter['slug']) {
            $q->where('slug', $filter['slug']);
        }

        if ($filter['date']) {
            $q->where('date', $filter['date']);
        }

        list($sortBy, $sortDir) = explode('-', $filter['sort']);
        $q->orderBy($sortBy, $sortDir);

        $items = $q->paginate(config('admin.item_per_page', 50));
        $items->appends($request->input());

        $this->layout
            ->setActiveSection('blog')
            ->setTitle(trans('admin/post.list'));

        return view('admin/post.list', compact('items', 'filter'));
    }

    /**
     *
     */
    public function add()
    {
        return $this->index();
    }

    /**
     * @param null $id
     */
    public function index($id = null) {
        $this->authorize('permission', 'blog.control');
        $post = null;

        if ($id) {
            $post = BlogPost::query()->find($id);
            $form = app(PostUpdateForm::class)->create($post);
        }
        else {
            $post = new BlogPost;
            $post->date = date('m/d/Y');
            $form = app(PostSaveForm::class)->create($post);
        }
        $data = [
            'edit_post' => $post,
            'form' => $form,
        ];

        $this->layout
            ->setActiveSection('blog')
            ->setTitle(trans($id ? 'admin/post.edit' : 'admin/post.add'))
            ->addBreadcrumb(trans('admin/post.list'), route('admin/post_list'));
        if ($id) {
            return view('admin/post.edit', $data);
        }
        return view('admin/post.add', $data);
    }

    /**
     * @param Request $request
     */
    public function save (Request $request) {
        $this->validate($request,[
            'title' => 'required|max:255',
            'image' => 'required|image|dimensions:min_width=1920',
            'slug' => 'unique:blog_posts|max:255',
            'view_count' => 'integer',
            'follow_count' => 'integer',
            'date' => 'required|date',
            'section' => 'required',
            'banner_id' => 'required',
        ],['image.dimensions' => trans('admin/post.dimensions')]);
        $post = new BlogPost;
        $post->add($request);
        return redirect()->route('admin/post_list');
    }

    /**
     * @param Request $request
     * @param $id integer
     */
    public function update (Request $request) {

        $this->validate($request,[
            'id' => 'required|exists:blog_posts|integer',
            'title' => 'required|max:255',
            'image' => 'image|dimensions:min_width=1920',
            'slug' => 'unique:blog_posts,slug,'.$request->id.'|max:255',
            'date' => 'required|date',
        ],['image.dimensions' => trans('admin/post.dimensions')]);

        $post = BlogPost::find($request->id);
        $post->update_post($request);
        return redirect()->route('admin/post_list');
    }
    public function delete ($id) {
        BlogPostBlogSection::where('blog_post_id', $id)->delete();
        BlogPost::where('id', $id)->delete();
        return redirect()->route('admin/post_list');
    }

    public function action(Request $request)
    {
        $this->authorize('permission', 'blog.control');

        $action = $request->input('action');
        $itemIds = $request->input('ids');
        if (!$itemIds) {
            return;
        }
        foreach ($itemIds as $itemId) {
            $item = BlogPost::query()->findOrFail($itemId);
            switch ($action) {
                /*case 'publish':
                    $item['is_published'] = true;
                    $item->published_at = new Carbon;
                    $item->save();
                    break;
                case 'hide':
                    $item['is_published'] = false;
                    $item->save();
                    break;*/
                case 'delete':
                {
                    $item->imageAllDelete();
                    BlogPostBlogSection::where('blog_post_id',$itemId)->delete();
                    $item->delete();
                }
                    break;
            }
        }
    }
}
