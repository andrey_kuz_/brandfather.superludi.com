<?php

namespace App\Http\Controllers\Auth;


use Socialite;
use Log;

use App\Models\User;
use App\Models\SocialAccount;
use App\Http\Controllers\Controller;
use App\Services\SocialAccountService;
use Illuminate\Support\Facades\Auth;

use AltSolution\Admin\Models\AclRole;
use AltSolution\Admin\Seo\SeoManagerInterface;

class SocialiteController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback(SocialAccountService $service, $provider = '')
    {
        try {
            $socialite_user = Socialite::driver($provider)->user();
			$social_user_email = $socialite_user->getEmail();
			$social_user_name = $socialite_user->getName();
			$social_user_avatar = $socialite_user->getAvatar();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
			return redirect(route('index'));
        }
		if(empty($social_user_name))
		{
			$social_user_name = 'the Unknown';
		}	
		
		$social_auth_error = '';
		
		if(empty($social_user_email))
		{
			$social_auth_error = trans('auth.error_getting_social_user_email');
		}
		
        $user = $service->getUser($socialite_user, $provider);
		if(!empty($user) && empty($social_auth_error))
		{
            auth()->login($user);
            $id = Auth::id();
            if ($id) {
                $user = User::find($id);
                if (!$user->active) {
                    Auth::logout();
                    return redirect('/');
                }
            }
            return redirect(route('video_list'));
        }
		
		$social_account = SocialAccount::whereProviderUserId($socialite_user->getId())->first();
		$user = User::where('email', $social_user_email)->first();
		if(empty($user) && !empty($social_account) && empty($social_auth_error))
		{
			$social_auth_error = [
				'request_phone' => [
					'name' => $social_user_name,
					'email' => $social_user_email,
					'avatar' => $social_user_avatar,
				],
			];
		}	
		
		if(!empty($user) && empty($social_auth_error) && !empty($social_account) && empty($social_account->user_id))
		{
			$social_account->user()->associate($user);
			$social_account->save();
			auth()->login($user);
            $id = Auth::id();
            if ($id) {
                $user = User::find($id);
                if (!$user->active) {
                    Auth::logout();
                    return redirect('/');
                }
            }
			return redirect(route('video_list'));
		}
		
		if(empty($social_auth_error))
		{
			$social_auth_error = trans('auth.error_retrieving_facebook_user');
		}
		
		return redirect(route('index'))->with(compact('social_auth_error'));
		
    }

}
