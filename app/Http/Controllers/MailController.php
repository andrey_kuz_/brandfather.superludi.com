<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\UniSenderService;

class MailController extends Controller
{
    public function send (Request $request){

        $this->validate($request,
            [
                'EMAIL' => 'required|email'
            ]
        );

        $unisender = new UniSenderService();
        $unisender->send($request->EMAIL);

    }
}
