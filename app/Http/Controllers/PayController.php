<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Models\Package;
use AltSolution\Admin\Seo\SeoManagerInterface;
use AltSolution\Admin\Models\Option;

class PayController extends Controller
{	
	public function success(Request $request)
    {
		$logger = app('pay.log');
		
		$order_number_raw = $request->input('order', '');
		list($user_id, $order_id) = Order::parserOrderId($order_number_raw);
		$user_id = intval($user_id);
		$order_id = intval($order_id);
		if($user_id <= 0 || (!empty(\Auth::user()) && \Auth::user()->id != $user_id) || $order_id <= 0)
		{
			$logger->info('Плохая успешная оплата', ['order_number_raw' => $order_number_raw]);
			return redirect(route('index'));
		}
		
		$user = !empty(\Auth::user()) ? \Auth::user() : User::active()->find($user_id);
		if(empty($user) || !($user instanceof User))
		{
			$logger->info('Успешная оплата: не найден пользователь', ['order_number_raw' => $order_number_raw]);
			return redirect(route('index'));
		}
		
		$order = Order::find($order_id);
		if(empty($order) || $order->user->id != $user_id)
		{
			$logger->info('Успешная оплата: не найден заказ', ['order_number_raw' => $order_number_raw]);
			return redirect(route('index'));
		}	
		
		if(empty($order->payed))
		{
			$no_payment_confirmation = 1;
			$logger->info('Успешная оплата: не получено подтверждение оплаты', ['order_number_raw' => $order_number_raw]);
			return redirect(route('index'))->with(compact('no_payment_confirmation'));
		}
		
		if(empty(\Auth::user()))
		{
			auth()->login($user);
		}
		
		$url_purchased_video = route('video_list');
		if(count($order->baskets) == 1)
		{
			if(!empty($order->baskets[0]->video->is_published))
			{	
				$url_purchased_video = route('video_detail', ['permalink' => $order->baskets[0]->video->permalink]);
			}	
		}
		
		/*turn off
		$package = Package::byName($order->package_name);
		$is_send = false;
		if(!empty($package['code']))
		{	
			$is_send = cms_send_template('PaySuccessPackage'.$package['code'], [
				'user.email' => $user->email,
				'user.name' => $user->name,
				'package.name' => $package['name'],
				'site.default.title' => app(SeoManagerInterface::class)->getDefaultTitle(),
			]);
		}	
		if(empty($is_send))
		{
			$logger->info('Успешная оплата: ошибка отправки письма', ['order_number_raw' => $order_number_raw]);
		}
		*/


		$logger->info('Успешная оплата выполнена', ['order_number_raw' => $order_number_raw]);
		return redirect(route('index'))->with(compact('url_purchased_video'));
    }

    public function fail()
    {
        $logger = app('pay.log');
		$payment_error = __FUNCTION__;
		
		$logger->error('PayController/fail Ошибка оплаты', ['user_id' => (!empty(\Auth::user()) ? \Auth::user()->id : 0)]);
		return redirect(route('index'))->with(compact('payment_error'));
    }

    public function callback(Request $request)
    {
        $logger = app('pay.log');
		
		$request_data = $request->input();
        $logger->debug('PayController/callback Platon callback', $request_data);

        $data = [];
        $params = ['sign', 'id', 'order', 'card', 'amount', 'currency', 'email', 'status', 'ip', 'name', 'city', 'address'];
        foreach($params as $param) {
            $data[$param] = $request->input($param);
        }
		
		if(empty($data['order']))
		{
            $logger->error('PayController/callback Пустой order id', ['request' => $request_data]);
			return response('ERROR');
		}	
		
		list($user_id, $order_id) = Order::parserOrderId($data['order']);
		$user_id = intval($user_id);
		$order_id = intval($order_id);
		if($user_id <= 0 || $order_id <= 0)
		{
			$logger->error('PayController/callback Плохой order id', ['request' => $request_data]);
			return response('ERROR');
		}	
		
		$user = User::active()->find($user_id);
		if(empty($user) || !($user instanceof User))
		{
			$logger->error('PayController/callback Неверный user id', ['request' => $request_data]);
			return response('ERROR');
		}	
		
		$order = Order::find($order_id);
		if(empty($order) || $order->user->id != $user_id)
		{
			$logger->error('PayController/callback Не найден заказ', ['request' => $request_data]);
			return response('ERROR');
		}
		
        // generate signature from callback params
        $pass = config('platon.pass');
        $sign = md5(strtoupper(
            strrev($data['email']) .
            $pass .
            $data['order'] .
            strrev(substr($data['card'], 0, 6) . substr($data['card'], -4))
        ));

        // verify signature
        if($data['sign'] !== $sign) {
            $logger->error('Platon-обработка стуса. Неверная подпись', ['request' => $request_data]);
            return response('ERROR');
        }

        switch ($data['status']) {
            case 'SALE':
				if($order->purchased($data)) {		
					$logger->info('PayController/callback Platon-обработка стуса. '.$data['status'], ['order_id' => $data['order']]);
				} else {
					$logger->error('PayController/callback Platon Ошибка обработки стуса. '.$data['status'], ['request' => $request_data]);
				}	
                break;
            case 'REFUND':
			case 'CHARGEBACK':	
				if($order->refund($data)) {		
					$logger->info('PayController/callback Platon-обработка стуса. '.$data['status'], ['order_id' => $data['order']]);
				} else {
					$logger->error('PayController/callback Platon Ошибка обработки стуса. '.$data['status'], ['request' => $request_data]);
				}	
                break;
            default:
                $logger->error('PayController/callback Platon-обработка стуса. Invalid callback data', ['request' => $request_data]);
                return response('ERROR');
        }
        $get_rate = Option::query();
        $option = Option::query();
        $order_update = Order::find($order_id);
        $sale = 0;
        $price_rate = $get_rate->where('name', 'course_price_rate')->pluck('value')->first();

        switch($order_update->package_name) {
            case 'GOOD':
                $sale = $option->where('name', 'course_package_cheap_price')->pluck('value')->first();
                break;
            case 'BEST':
                $sale = $option->where('name', 'course_package_good_price')->pluck('value')->first();
                break;
            case 'PREMIUM':
                $sale = $option->where('name', 'course_package_premium_price')->pluck('value')->first();
                break;
        }
        $order_update->price = (int)($sale * $price_rate);
        $order_update->save();

        return response('OK');
    }

}
