<?php

namespace App\Http\Controllers;

use App\Models\BlogBanner;
use App\Models\BlogPost;
use App\Models\BlogSection;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\Cookie;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class BlogController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = $this->paginate();
        $sections = BlogSection::all();
        $mainPage = env('APP_DOMAIN');

        $meta['title'] = 'СУПЕРЛЮДИ База Знаний';
        $meta['keywords'] = '';
        $meta['description'] = '';

        return view('blog.main', compact('data', 'sections', 'mainPage', 'meta'));
    }

    /**
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function paginate($page = 0)
    {
        $data['posts'] = BlogPost::orderBy('date', 'desc')->where('draft', 0)->limit(15)->offset(14 * $page)->get();
        $data['banner'] = BlogBanner::where('main_page', true)->get();

        $meta['title'] = 'СУПЕРЛЮДИ База Знаний';
        $meta['keywords'] = '';
        $meta['description'] = '';

        if ($page) {
            if (count($data['posts'])) {
                return view('blog.parts.main-posts', compact('data', 'meta'));
            }
            return null;
        }
        return $data;
    }

    /**
     * @param $section_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSection($section_name)
    {

        $sections = BlogSection::all();
        $mainPage = env('APP_DOMAIN');
        $section = BlogSection::where('slug', $section_name)->first();
        $posts = $section->posts()->where('draft', 0)->orderBy('date', 'desc')->get();

        $meta['title'] = $section->title . ' | СУПЕРЛЮДИ База Знаний';
        $meta['keywords'] = '';
        $meta['description'] = '';

        return view('blog.section', compact('section', 'posts', 'sections', 'mainPage', 'meta'));
    }

    /**
     * @param $section_name
     * @param $post_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showPost($section_name, $post_name)
    {
        $mainPage = env('APP_DOMAIN');
        $sections = BlogSection::all();
        $post_view_count = BlogPost::where('slug', $post_name)->first();
        if (!Cookie::has('idpage' . $post_view_count->id)) {
            $post_view_count->view_count = $post_view_count->view_count + 1;
            setcookie('idpage' . $post_view_count->id, $post_view_count->id, time() + 60 * 60 * 24 * 30);
        }
        $post_view_count->save();
        $post = BlogPost::where('slug', $post_name)->first();
        if ($post->content) {
            $html = HtmlDomParser::str_get_html($post->content);
            foreach ($html->find('iframe') as $iframe) {
                if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $iframe->src, $match)) {
                    $Youtube_video_id = $match[1];
                    $iframe->src = $iframe->src . '?autoplay=1&rel=0';
                    $iframe->outertext = '<div class="adaptive-iframe" id="' . $Youtube_video_id . '">' . $iframe->outertext . '</div>';
                }
            }
            $post->content = $html;
        }
        $currentID = $post->id;
        $mostViews = $this->mostViews(0, $currentID);
        if ($post->banner_id)
            $banner = BlogBanner::find($post->banner_id);

        $meta['title'] = $post->title;
        $meta['keywords'] = '';
        $meta['description'] = '';
        foreach ($post->sections as $section) {
            if ($section->slug == $section_name) {
                return view('blog.post', compact('post', 'banner', 'mostViews', 'section_name', 'sections', 'mainPage', 'meta', 'currentID'));
            }
        }
        return back();
    }

    /**
     * Ajax method to check social count
     */
    public function socialCount(Request $request)
    {
        $mainPage = env('APP_DOMAIN');
        $socialCount = array();
        $vkontakte = "";
        $pinterest = "";
        $facebook = "";
        $pinterest_count = 0;
        $facebook_count = 0;
        $vkontakte_count = 0;
        $error = 0;
        try {
            $vkontakte = $this->file_get_contents_timeout('https://vk.com/share.php?act=count&index=1&url=https://blog.'. $mainPage . '/' . $request->section_name . '/' . $request->post_name);
            if ($vkontakte) {
                $pos = strpos($vkontakte, ",");
                $vkontakte_count = intval(substr($vkontakte, $pos + 1, -2));
            }
        }
        catch (\Exception $e) {
            $error++;
            $view_log = new Logger('Social Count Error');
            $view_log->pushHandler(new StreamHandler('../storage/logs/social_log_error', Logger::INFO));
            $view_log->addInfo('Error on social network vkontakte: '.$e->getMessage());
        }

        try {
            $pinterest = $this->file_get_contents_timeout('https://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url=https://blog.' . $mainPage . '/' . $request->section_name . '/' . $request->post_name);
            if ($pinterest) {
                $pinterest = substr($pinterest, 13, -1);
                $pinterest = json_decode($pinterest, true);
                $pinterest_count = $pinterest['count'];
            }
        }
        catch (\Exception $e) {
            $error++;
            $view_log = new Logger('Social Count Error');
            $view_log->pushHandler(new StreamHandler('../storage/logs/social_log_error', Logger::INFO));
            $view_log->addInfo('Error on social network pinterest: '.$e->getMessage());
        }

        try {
            $facebook = $this->file_get_contents_timeout('https://graph.facebook.com/?id=https://blog.' . $mainPage . '/' . $request->section_name . '/' . $request->post_name);
            if ($facebook) {
                $facebook = json_decode($facebook, true);
                $facebook_count = intval($facebook['share']['share_count']);
            }
        }
        catch (\Exception $e) {
            $error++;
            $view_log = new Logger('Social Count Error');
            $view_log->pushHandler(new StreamHandler('../storage/logs/social_log_error', Logger::INFO));
            $view_log->addInfo('Error on social network facebook: '.$e->getMessage());
        }

        $post = BlogPost::where('slug', $request->post_name)->first();
        if ($facebook_count) {
            $post->share_facebook_count = $facebook_count;
            $socialCount['facebook'] = $facebook_count;
        }
        if ($pinterest_count) {
            $post->share_pinterest_count = $pinterest_count;
            $socialCount['pinterest'] = $pinterest_count;
        }
        if ($vkontakte_count) {
            $post->share_vkontakte_count = $vkontakte_count;
            $socialCount['vkontakte'] = $vkontakte_count;
        }
        $post->save();
        $view_log = new Logger('Social Count Log');
        $view_log->pushHandler(new StreamHandler('../storage/logs/social_log', Logger::INFO));
        if ($socialCount && !$error) {
            $view_log->addInfo(json_encode($socialCount));
        } else {
            $view_log->addInfo('No one shared the article');
        }
        if ($error) {
            return response('Social Count not available', 503 );
        } else {
            return ($facebook_count + $pinterest_count + $vkontakte_count);
        }
    }

    /**
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function mostViews($page, $idСurrent)
    {
        if ($idСurrent) {
            $mostViews = BlogPost::where('id', '!=', $idСurrent)->where('draft', 0)->orderBy('view_count', 'desc')->limit(5)->offset(4 * $page)->get();
        } else {
            $mostViews = BlogPost::orderBy('view_count', 'desc')->where('draft', 0)->limit(5)->offset(4 * $page)->get();
        }
        if ($page) {
            if (count($mostViews)) {
                return view('blog.parts.post-most-view', compact('mostViews'));
            }
            return null;
        }
        return $mostViews;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function searchPost(Request $request)
    {
        $mainPage = env('APP_DOMAIN');
        $sections = BlogSection::all();
        $result = BlogPost::SearchByKeyword($request->search)->where('draft', 0)->get();
        $search = $request->search;
        return view('blog.search', compact('result', 'search', 'sections', 'mainPage'));
    }

    public function searchPage()
    {
        $mainPage = env('APP_DOMAIN');
        $sections = BlogSection::all();
        return view('blog.search', compact('sections', 'mainPage'));
    }

    public function file_get_contents_timeout($filename, $timeout = 1)
    {
        if (strpos($filename, "://") === false) return file_get_contents($filename);
        if (!function_exists("curl_init")) return false;
        $session = curl_init($filename);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($session, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($session, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible)");
        $result = curl_exec($session);
        curl_close($session);
        return $result;
    }
}
