<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use App\Models\Video;
use App\Models\User;
use App\Models\Pay;
use App\Models\Package;

class IndexController extends Controller
{
    public function index()
    {
		$social_auth_error = User::socialAuthError();
		
		if(!empty($social_auth_error))
		{
			if(is_array($social_auth_error) && !empty($social_auth_error['request_phone']['email']))
			{
				$request_phone = $social_auth_error['request_phone'];
				$request_phone['redirect'] = route('video_list');
				view()->share('popup_url', '#modal-phone');
				$social_auth_error = '';
			}
			else
			{
				view()->share('popup_url', '#modal-login');
			}	
		}
		
		$payment_error = Pay::fail();
		if(!empty($payment_error))
		{
			view()->share('popup_url', '#modal-buy-course-second-step-fail');
		}
		
		$no_payment_confirmation = Pay::noConfirmation();
		if(!empty($no_payment_confirmation))
		{
			view()->share('popup_url', '#modal-buy-course-second-step-no-payment-confirmation');
		}
		
		$url_purchased_video = Pay::success();
		if(!empty($url_purchased_video))
		{
			view()->share('popup_url', '#modal-buy-course-3');
			view()->share('url_purchased_video', $url_purchased_video);
		}
		
		$package_to_pay = Package::toPay();
		if(!empty($package_to_pay))
		{
			view()->share('order_to_pay', $package_to_pay);
		}
		
		$videos = Video::available()->sorting()->get();
		$unpaid_videos = [];
		$paid_videos = [];
		if(!empty(\Auth::user()))
		{	
			$unpaid_videos = \Auth::user()->unpaidVideos();
			$paid_videos = \Auth::user()->paidVideos();
		}	
		
		$packages = Package::availables();
		$episode_count = count($videos);
		$episode_word = trans_choice('site.episod_decline', $episode_count);
		
		$course_expires = cms_option('course_expires');
		$course_expires_word = trans_choice('site.months_left_decline', $course_expires);
		
		
        cms_seo('index');
        $blog_post = BlogPost::orderBy('date','desc')->take(3)->get();

        return view('index', compact('videos', 'packages', 'episode_count',
			'episode_word', 'social_auth_error', 'request_phone',
			'unpaid_videos', 'paid_videos', 'course_expires', 'course_expires_word', 'blog_post'));
    }
}
