<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Foundation\Application;
use Illuminate\Contracts\Encryption\Encrypter;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
	
	public function __construct(Application $app, Encrypter $encrypter)
	{
		$this->except[] = \URL::route('pay_callback', [], false);
        $this->except[] = \URL::route('access', [], false);

		parent::__construct($app, $encrypter);
	}
}
