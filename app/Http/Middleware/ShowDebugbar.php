<?php

namespace App\Http\Middleware;

use Barryvdh\Debugbar\LaravelDebugbar;
use Closure;
use Illuminate\Contracts\Auth\Access\Gate;

class ShowDebugbar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /** @var Gate $gate */
        $gate = app(Gate::class);
        
        if ($request->ajax() || !$gate->check('permission', 'debug')) {

            /** @var LaravelDebugbar $debugBar */
            $debugBar = app('debugbar');
            // disable instead of enable, cuz otherwise it not register required middleware
            $debugBar->disable();
        }
        
        return $next($request);
    }
}
