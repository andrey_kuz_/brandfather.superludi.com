<?php

namespace App\Emails;

use AltSolution\Admin\EmailTemplate\Template;

class MadePaid extends Template
{
    protected $view = 'emails.made-paid';
    protected $name = 'MadePaid';
    protected $description = 'Update user to paid in admin panel';

    public function init()
    {
        $this->setLegend([
            'user.email' => "User's email",
            'user.name' => "User's name",
            'user.password' => "User's password",
            'site.default.title' => 'Default site title',
        ]);
        $this->setNameTo('');
        $this->setNameFrom(config('mail.from.name'));
    }
}