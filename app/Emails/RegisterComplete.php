<?php

namespace App\Emails;

use AltSolution\Admin\EmailTemplate\Template;

class RegisterComplete extends Template
{
    protected $view = 'emails.register-complete';
    protected $name = 'RegisterComplete';
    protected $description = 'Register complete user email template';

    public function init()
    {
        $this->setLegend([
            'user.email' => "User's email",
            'user.name' => "User's name",
            'user.password' => "User's password",
			'site.default.title' => 'Default site title',
        ]);
        $this->setNameTo('');
        $this->setNameFrom(config('mail.from.name'));
    }
}