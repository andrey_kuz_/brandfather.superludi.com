<?php

return [
    'list' => 'List examples',
    'add' => 'Add example',
    'add_btn' => 'Add',
    'edit' => 'Edit example',
    'sort' => 'Sort examples',
    'sort_btn' => 'Sort',
    'saved' => 'Example saved',
    'sorted' => 'Examples sorted',

    'visible' => 'Visibility',
    'published' => 'Published',
    'hidden' => 'Hidden',

    'f_title' => 'Title',
    'f_permalink' => 'Permalink',
    'f_published_at' => 'Date',
    'f_published' => 'Published',
    'f_published_on' => 'published',
    'f_image' => 'Image',
    'f_content' => 'Content',
    'h_image' => 'image size is 100x100 px'
];