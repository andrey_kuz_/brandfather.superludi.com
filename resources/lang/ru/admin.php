<?php

return [
    'me_content_view' => 'Страница контента',

    // example

    'me_example_list' => 'Примеры',

    'ps_example' => 'Примеры',
    'p_example_list' => 'Список примеров',
    'p_example_edit' => 'Добавить \ Редактировать примеры',
    'p_example_delete' => 'Удалить примеры',
    
    'm_example' => 'Примеры',
    'mi_example_list' => 'Список примеров',

    'ss_example' => 'Примеры',
    's_text' => 'Текст',

    // video
	
	'me_video_list' => 'Видео',

    'ps_video' => 'Видео',
    'p_video_list' => 'Список видео',
    'p_video_edit' => 'Добавить \ Редактировать видео',
    'p_video_delete' => 'Удалить видео',
    
    'm_video' => 'Видео',
    'mi_video_list' => 'Список видео',

    'ss_video' => 'Видео',
	's_video_text' => 'Текст',
    
	//order
	
	'm_order' => 'Заказы',
	'ps_order' => 'Заказы',
	'me_order_list' => 'Заказы',
	'p_order_list' => 'Просмотр заказов',
	'mi_order_list' => 'Список заказов',

    //Secton

    'm_blog' => 'Блог',
    'mi_blog_section_list' => 'Разделы блога',
    'mi_blog_post_list' => 'Посты блога',
    'mi_blog_banner_list' => 'Баннеры блога',
    'm_section' => 'Разделы',
    'ps_section' => 'Разделы',
    'me_section_list' => 'Разделы',
    'p_section_edit' => 'Добавить \ Редактировать разделы',
    'mi_|section_delete' => 'Список разделов',
];