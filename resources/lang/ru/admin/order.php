<?php

return [
	'select_payed' => 'Оплачен',
	'payed_yes' => 'Да',
	'payed_no' => 'Нет',
	'list' => 'Список заказов',
	'number' => 'Номер',
	'user' => 'Пользователь',
	'video' => 'Видео',
	'history' => 'История',
	'payed_at' => 'Время оплаты',
	'payed' => 'Оплачен',
	'package_name' => 'Пакет',
];