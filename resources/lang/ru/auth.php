<?php

return [
    'error_create_user' => 'Ошибка создания пользователя',
	'error_sending_message' => 'Ошибка отправки письма',
	'error__retrieving_user_role' => 'Ошибка получения роли пользователя',
	'error_getting_social_user_email' => 'Ошибка получения Email пользователя из соц. сети',
	'login_successful' => 'Вход выполнен успешно',
	'please_check_entered_data' => 'Ошибка входа. Пожалуйста, проверьте введенные данные',
	'site_user_with_email_not_found' => 'Пользователь сайта с email ":email" не найден. Вход отклонен',
	'error_retrieving_facebook_user' => 'Ошибка получения пользователя Facebook',
	'please_make_purchase' => 'Чтобы выполнить вход, пожалуйста, сделайте покупку',
	'too_many_login_attempts' => 'Слишком много попыток входа в систему. Повторите попытку через :seconds секунд',
	'user_successfully_created' => 'Пользователь успешно создан. Информация для входа отправленна на почту ":email"',
	'email_unique' => 'Такое значение поля email уже существует. Пожалуйста, выполните вход.',
	'user_found' => 'Пользователь успешно найден',
	'user_already_exists' => 'Пользователь с таким email уже существует',
    'user_access_denied' => 'Вход запрещён, вы заблокированы',
];