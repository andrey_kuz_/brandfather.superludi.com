<?php

return [
    'email_not_found' => 'Пользователь с таким email не найден',
	'error_generating_password' => 'Ошибка сохранения нового пароля',
	'error_sending_message' => 'Ошибка отправки письма',
];