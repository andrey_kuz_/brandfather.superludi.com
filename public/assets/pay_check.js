$('form ').submit(function (e) {
   var pay = $('input[name="paid"]').is(":checked");
   var id = $('input[name="id"]').val();
   var password = $('input[name="password"]').val();
   var password_confirmation = $('input[name="password_confirmation"]').val();
   if (!pay && !password && !id) {
       $('input[name="password"]').parent().parent().removeClass('has-success').addClass('has-error');
       $('input[name="password"]').next().removeClass('glyphicon-ok').addClass('glyphicon-remove');
       $('input[name="password"]').next().next().show();
       e.preventDefault();
   } else {
       $('input[name="password"]').parent().parent().removeClass('has-error').addClass('has-success');
       $('input[name="password"]').next().next().hide();
       $('input[name="password"]').next().removeClass('glyphicon-remove').addClass('glyphicon-ok');
   }
    if (!pay && !password_confirmation && !id) {
        $('input[name="password_confirmation"]').parent().parent().removeClass('has-success').addClass('has-error');
        $('input[name="password_confirmation"]').next().removeClass('glyphicon-ok').addClass('glyphicon-remove');
        $('input[name="password_confirmation"]').next().next().show();
        e.preventDefault();
    } else {
        $('input[name="password_confirmation"]').parent().parent().removeClass('has-error').addClass('has-success');
        $('input[name="password_confirmation"]').next().next().hide();
        $('input[name="password_confirmation"]').next().removeClass('glyphicon-remove').addClass('glyphicon-ok');
    }
});