//custom Wysiwyg
function customWysiwyg(){
    tinymce.init({
        selector: "#content",
        templates : [
            {"title": "Last Block", "description": "Last Block", "content": "<div class='last_paragraph'><p>Введите текст для последнего абзаца</p></div>"},
        ],
        theme: "modern",
        height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        // toolbar_items_size: 'small',
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview code media | forecolor backcolor emoticons | codesample',
        //menubar: false,
        //statusbar: false,
        relative_urls: false,
        verify_html : false,
        language: CMS.locale(),
        file_browser_callback: elFinderBrowser,
        //add_form_submit_trigger: false,
        //add_unload_trigger

        init_instance_callback: function (editor) {
            editor.on('Change', function (e) {
                // fix validation
                editor.save();
                $(editor.getElement()).trigger('input');
            });
        }
    });
    // more @ https://www.tinymce.com/docs/demo/classic/
}